#include <stdio.h>

int main()
{
    printf("ljf");
    printf("xiena"); 
    printf("hejiong");
    printf("");

    printf("ono");
	printf("o");
	printf("\n");
	printf("    o");
	printf("\n");

	//1： 屏幕上显示hi hi hi hi
	//1.1 没学循环时的方法，4步才能做完
	printf("hi ");
    printf("hi ");
    printf("hi ");
    printf("hi ");

	//1.2 学循环时的方法
    for(int i=1; i<=4; i++){
		printf("hi ");
	}

	//2： 屏幕上显示1 2 3 4
	//1.1 没学循环时的方法，4步才能做完
    printf("%d", 1);
    printf("%d", 2);
    printf("%d", 3);
    printf("%d", 4);

	//1.2 学循环时的方法
    for(int i=1; i<=4; i++)
        printf("%d", i);

	return 0;
}