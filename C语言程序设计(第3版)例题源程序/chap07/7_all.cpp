#include<stdio.h>

/* 【例7-14】进制转换。输入一个以’#’为结束标志的字符串（少于10个字符），滤去所有的非十六进制字符
（不分大小写），组成一个新的表示十六进制数字的字符串，输出该字符串并将其转换为十进制数后输出。 */
/* 进制转换 */

int ex_714_filter_hex(void)
{
    int i,k;
    char hexad[80],str[80];
    long number;

    /* 输入字符串 */
    printf("Enter a string: ");      /* 输入提示 */
    i = 0;
	while((str[i] = getchar( )) != '#') {     /* 输入结束符 '#' */
		i++; 
	}
    str[i] = '\0';                /* 将字符串结束符 '\0' 存入数组 */

    /*滤去非16进制字符后生成新字符串hexad */
    k= 0;                     /* k：新字符串hexad的下标 */
    for(i = 0; str[i] != '\0'; i++)
        if(str[i]>='0'&&str[i]<='9'||str[i]>='a'&&str[i]<='f'||str[i]>='A'&&str[i]<='F'){ 
            hexad[k] = str[i];       /* 放入新字符串 */
            k++;         
        }
    hexad[k] = '\0';                 /* 新字符串结束标记 */

    /* 输出十六进制新字符串 */
    printf("New string:");
    for( i= 0; hexad[i] != '\0'; i++) 
        putchar(hexad[i]);
    printf("\n");

    /* 转换为十进制整数 */ 
    number = 0;                  /* 存放十进制数，先清0 */
    for(i = 0; hexad[i] !='\0'; i++){      /* 逐个转换 */
        if(hexad[i] >= '0' &&hexad[i] <= '9')
            number = number * 16 + hexad[i] - '0';
        else if(hexad[i] >= 'A' &&hexad[i] <= 'F')
            number = number * 16 + hexad[i] - 'A' + 10;
        else if(hexad[i] >= 'a' &&hexad[i] <= 'f')
            number = number * 16 + hexad[i] - 'a' + 10;
    }
    printf("Number = %ld\n",number);   /* 输出十进制值 */

    return 0;
}    

/* 【例7-13】输入一个以回车符为结束标志的字符串（少于10个字符），
提取其中的所有数字字符（'0'……'9'），将其转换为一个十进制整数输出。*/
int ex_713_extract_number(void)
{
    int i, number;	
    char str[10];

    /* 输入字符串 */
    printf("Enter a string: ");    	/* 输入提示 */
    i = 0;
	while((str[i] = getchar( )) != '\n') {
		i++; 
	}
    str[i] = '\0';                      /* 将结束符 '\0' 存入数组 */

    /* 逐个判断是否为数字字符，并进行转换 */
    number= 0;                          /* 存放结果，先清0 */
    for(i = 0; str[i] != '\0'; i++)     /* 循环条件：str[i]不等于 '\0' */
        if(str[i] >= '0' &&str[i]<='9')	/* 是数字字符 */
            number=number*10+str[i]-'0';   /* 转换成数字 */

    printf("digit=%d\n",number);

    return 0;
}    

/* 【例7-12】输入一个以回车符为结束标志的字符串（少于80个字符），统计其中数字字符（'0'……'9'）的个数。*/
/* 统计字符串中数字字符的个数 */
int ex_712_count(void)
{
    int count, i;
    //char str[80];
	char str[80] = {'a', '2', '4', 'b', '\0' };

 //   /* 输入字符串 */
 //   printf("Enter a string: ");            /* 输入提示 */
 //   i = 0;
	//while((str[i] = getchar( )) != '\n') {
	//	rewind(stdin);
 //       i++; 
	//}
 //   str[i] = '\0';                        /* 将结束符 '\0' 存入数组 */

    /* 统计字符串中数字字符的个数 */
    count = 0;
	for(i = 0; str[i] != '\0'; i++) {      /* 循环条件：str[i] 不等于 '\0' */
        if(str[i] <= '9' &&str[i] >= '0') 
            count++;
	}
    printf("count = %d\n", count);

    return 0;
}   

/* 【例7-11】输入一个以回车符为结束标志的字符串（少于80个字符），判断该字符串是否为回文。
回文就是字符串中心对称，如“abcba”、“abccba”是回文，“abcdba”不是回文。 */
/*判断字符串是否为回文*/
int ex_711_palindrome(void)
{
    int i, k;
    char line[80];

    /* 输入字符串 */
    printf("Entera string: ");   /* 输入提示 */
    k=0;
	while((line[k]=getchar())!='\n'){
		k++;
	}
    line[k]='\0';

    /* 判断字符串line是否为回文 */
    i =0;     		/* i是字符串首字符的下标 */
    k =k -1;		/* k是字符串尾字符的下标 */
    /* i和k两个下标从字符串首尾两端同时向中间移动，逐对判断对应字符是否相等 */
    while(i< k){
		if(line[i]!=line[k])	/* 若对应字符不相等，则提前结束循环*/
			break;
		i++;
		k--;
    }
    if( i>= k)   /*判断for循环是否正常结束，若是则说明字符串是回文 */
        printf("It is a plalindrome\n");
    else	/* for循环非正常结束，说明对应字符不等 */
        printf("It is not a plalindrome\n");

    return 0;
}    

/* 【例7-10】定义函数day_of_year(year, month, day)，计算并返回年year、月month和日day对应的是该年的第几天。 */
/* 计算某个日期对应该年的第几天 */
int day_of_year(int year, int month, int day)
{
    int k, leap;
    int tab[2][13] = {					/* 数组初始化，将每月的天数赋给数组 */
        {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}, 
        {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31} 
    };

    /* 判断闰年，当year是闰年时，leap=1；当year是非闰年时，leap=0 */
    leap = (year % 4 == 0 && year%100!=0 || year%400==0); 

    /* 计算天数 */
    for(k = 1; k < month; k++)
       day = day + tab[leap][k];

    return day; 
}    

/* 【例7-9】输入一个正整数n (1＜n≤6)，根据下式生成一个n*n的方阵，将该方阵转置（行列互换）后输出。 */
/* 方阵转置 */
int ex_79_transpose_matrix(void)
{
    int i, j, n, temp;
    int a[6][6];

    /* 给二维数组赋值 */
    printf("Enter n: "); 
    scanf("%d", &n);
    for(i = 0; i < n; i++)        		/* 行下标是外循环的循环变量 */        
        for(j = 0; j < n; j++)    		/* 列下标是内循环的循环变量 */       
            a[i][j] = i * n + j + 1; 	/* 给数组元素赋值 */
        
    /* 行列互换*/
    for(i = 0; i < n; i++)
        for(j = 0; j < n; j++)
            if(i <= j){         	    /* 只遍历上三角阵 */
                temp = a[i][j]; 	    /* 以下3句交换 a[i][j] 和 a[j][i] */
                a[i][j] = a[j][i];
                a[j][i] = temp;
            }

    /* 按矩阵的形式输出a */
    for(i = 0; i < n; i++){     	     /* 针对所有行的循环 */
        for(j = 0; j < n; j++)   	     /* 输出第i行的所有元素 */      
            printf("%4d", a[i][j]); 
        printf("\n");      				/* 换行 */         
    }

    return 0;
}    

/* 【例7-8】定义一个3*2的二维数组a，数组元素的值由下式给出，按矩阵的形式输出a。 */
/* 按矩阵的形式输出二维数组 */
int ex_78_printf_2d_array(void)
{
    int i, j;
    int a[3][2];    /* 定义1个二维数组a */

    /* 给二维数组赋值 */   
    for(i = 0; i < 3; i++)      		/* 行下标是外循环的循环变量 */
        for(j = 0; j < 2; j++)   		/* 列下标是内循环的循环变量 */
            a[i][j] = i + j;    		/* 给数组元素赋值 */

    /* 按矩阵的形式输出a */
    for(i = 0; i < 3; i++){     		/* 针对所有行的循环 */
        for(j = 0; j < 2; j++)  		/* 输出第i行的所有元素 */
            printf("%4d", a[i][j]); 
        printf("\n");        			/* 换行 */
    }

    return 0;
}   

/* 【例7-7】将一个3 * 2的矩阵存入1个3 * 2的二维数组中，找出最大值以及它的行下标和列下标，并输出该矩阵。 */
/* 找出矩阵中的最大值及其行下标和列下标 */
int ex_77_max_2d_array(void)
{
    int col, i, j, row;
    int a[3][2];

    /* 将输入的数存入二维数组 */   
    printf("Enter 6 integers: \n");			/* 提示输入6 个数 */
    for(i = 0; i < 3; i++)
        for(j = 0; j < 2; j++)       
            scanf("%d", &a[i][j]); 

    /* 按矩阵的形式输出二维数组a */
    for(i = 0; i < 3; i++){
        for(j = 0; j < 2; j++)
            printf("%4d", a[i][j]); 
        printf("\n");
    }

    /* 遍历二维数组，找出最大值 a[row][col] */
    row = col = 0;							/* 先假设 a[0][0] 是最大值 */
    for(i = 0; i < 3; i++) 
       for(j = 0; j < 2; j++)
          if(a[i][j] > a[row][col]){		/* 如果 a[i][j] 比假设值大 */
              row = i;						/* 再假设 a[i][j] 是新的最大值 */
              col = j; 
          }
    printf("max = a[%d][%d] = %d\n", row, col, a[row][col]);

    return 0;
}   

/*【例7-6】调查电视节目欢迎程度。某电视台要进行一次对该台8个栏目（设相应栏目编号为1~8）的受欢迎情况，
共调查了1000位观众，现要求编写程序，输入每一位观众的投票，每位观众只能选择一个最喜欢的栏目投票，统计输出各栏目的得票情况。*/
/* 投票情况统计 */
int ex_76_vote( void ) 
{
    int count[9];							/* 设立数组，栏目编号对应数组下标 */
    int i,response;

    for(i = 1;i <= 8;i++)
        count[i] = 0;						/* 各栏目计数器清0 */
    for( i = 1;i <= 10;i++) {				/* 为调试运行方便，可把1000改小，如3 */
        printf("input your response: ");	/* 输入提示 */
        scanf("%d",&response);
        if(response < 1 || response > 8)	/* 检查投票是否有效*/
            printf("this is a bad response: %d\n",response);
        else
            count[response]++;				/* 对应栏目得票加1 */
    }

    printf("result:\n");					/* 输出各栏目得票情况 */
    for(i = 1;i <= 8;i++)
        printf("%4d%4d\n",i,count[i]);	

    return 0;
}

/* 【例7-5】选择排序法。输入一个正整数n (1<n≤10)，再输入n个整数，用选择法将它们从小到大排序后输出。 */
/* 选择法排序 */
int ex_75_sort_select(void)
{
    int i, index, k, n, temp;
    int a[10];         	/* 定义1个数组a，它有10个整型元素*/

    printf("Enter n: ");                /* 提示输入n */
    scanf("%d", &n);
    printf("Enter %d integers: ", n);   /* 提示输入n 个数 */
    for(i = 0; i < n; i++)				/* 将输入数依次赋给数组a的n个元素a[0]～a[n-1] */   
        scanf("%d", &a[i]);
    /* 对n个数排序 */
    for(k = 0; k < n-1; k++){
        index = k;						/* index存放最小值所在的下标 */
		for(i = k + 1; i < n; i++){		/* 寻找最小值所在下标 */
            if(a[i] < a[index])  
				index = i;
		}

        temp = a[index];				/* 最小元素与下标为k的元素交换 */
        a[index] = a[k];
        a[k] = temp;
    }

    printf("After sorted: ");		/* 输出n个数组元素的值 */
    for(i = 0; i < n; i++)       
        printf("%d ", a[i]);
    
    return 0;
} 

/* 【例7-4】输入一个正整数n (1＜n≤10)，再输入n个整数，将它们存入数组a中。
（1）输出最小值和它所对应的下标。
（2）将最小值与第一个数交换，输出交换后的n个数。
 */

/* 找出数组的最小值和它所对应的下标 */
int ex_74_min_array(void)
{
    int i, index, n;
    int a[10];    

    printf("Enter n: ");            	    /* 提示输入n */
    scanf("%d", &n);

    printf("Enter %d integers: ", n); 	    /* 提示输入n 个数 */
    for(i = 0; i < n; i++)  
        scanf("%d", &a[i]);
   /* 找最小值a[index] */
    index = 0;								/* 假设a[0]是最小值，即下标为0的元素最小 */
    for(i = 1; i < n; i++)  
        if(a[i] < a[index])					/* 如果 a[i] 比假设的最小值还小 */
            index = i;						/* 再假设 a[i] 是新的最小值，即下标为 i 的元素最小 */
    /* 输出最小值和对应的下标 */
    printf("min is %d \t sub is %d\n", a[index], index);   /* 第19行 */
	
	int s1 = 1, s2 = 0;
	//s1 s2 互换如下，可以吗？
	s1 = s2;
	s2 = s1;
	
	int w1 = 1, w2 = 0;
	//w1 w2互换如下
	int tmp;
	tmp = w1;
	w1 = w2;
	w2 = tmp;
	
	//前面数组中最小值找到后和数组第一个值互换
	tmp = a[index];
	a[index] = a[0];
	a[0] = tmp;	

    return 0;
}

/* 【补例7-6】二分查找法。设已有一个10个元素的整形数组a，且按值从小到大有序。输入一个整数x，然后在数组中查找x，如果找到，输出相应的下标，否则，输出"Not Found"。 */
int ex_76_find_number(void)   
{
    int a[10] = {1,2,3,4,5,6,7,8,9,10}; /* 有序数组 */
    int low,high,mid,n = 10,x;

    printf("Enter x：");                /* 提示输入x */
    scanf("%d",&x);                

    low = 0; high = n - 1;              /* 开始时查找区间为整个数组 */
    while (low <= high)  {              /* 循环条件 */
        mid = (low + high) / 2;         /* 中间位置 */
        if (x == a[mid])
            break;                      /* 查找成功，中止循环 */
        else if (x < a[mid])
            high = mid - 1;             /* 前半段，high前移 */        
        else       
            low = mid + 1;              /* 后半段，low后移 */
    }    
    if(low <= high)   
        printf("Index is %d \n",mid);
    else 
        printf( "Not Found\n");
		
    return 0;
}

/* 【例7-3】顺序查找法。输入5个整数，将它们存入数组a中，再输入1个数x，
然后在数组中查找x，如果找到，输出相应的最小下标，否则，输出"Not Found"。 */
/* 在数组中查找一个给定的数 */
int ex_73_find_number(void)
{
    int i, flag, x;
    int a[5];    

    printf("Enter 5 integers: ");            /* 提示输入5 个数 */
    for(i = 0; i < 5; i++) 
        scanf("%d", &a[i]);
    printf("Enter x: ");                     /* 提示输入 x */
    scanf("%d", &x);
    /* 在数组a中查找x */
    flag = 0;                                /* 先假设x不在数组a中，置flag为0 */
    for(i = 0; i < 5; i++) 
        if(a[i] == x){                       /* 如果在数组a中找到了x */
            printf("Index is %d\n", i);      /* 输出相应的下标 */
            flag = 1;                        /* 置flag为1，说明在数组a中找到了x */
            break;                           /* 跳出循环 */
        }
    if(flag == 0)                            /* 如果flag为0，说明x不在a中 */
        printf("Not Found\n");

    return 0;
}   


/* 【例7-2】利用数组计算斐波那契数列的前10个数 */
int ex_72_Fibonacci(void)
{
    int i;
    int fib[20] = {1, 1};			/* 数组初始化，生成斐波那契数列前两个数 */

    /* 计算斐波那契数列剩余的8个数 */
    for(i = 2; i < 10; i++)    
        fib[i] = fib[i - 1] + fib[i - 2];

    /* 输出斐波那契数列 */
    for(i = 0; i < 10; i++){ 
        printf("%6d", fib[i]);
        if((i + 1) % 5 == 0)		/* 每输出5个数就换行 */
            printf("\n");  
    }

    return 0;
}  

/*【例7-1】输入4个整数，计算这些数的平均值，再输出所有大于平均值的数。*/
int ex_71_average(void)
{
   int i; 
   double average, sum;       /* average存放平均值，sum保存数据之和 */
   int a[4];    	      /* 定义1个数组a，它有4个整型元素*/

   printf("Enter 4 integers: ");	/* 提示输入4个数 */
   /* 将输入数依次赋给数组a的4个元素a[0]～a[9]（如图7.1所示），并求和*/
   for(i =0; i<4; i++){
      scanf ("%d", &a[i]); 
   }

   sum = 0;
   for(i =0; i<4; i++){
      sum = sum + a[i]; 
   }
   average = sum / 4;	     /* 求平均值*/
   printf("average = %.2f\n", average);
   printf(">average:");
   for(i = 0; i<4; i++){    /* 逐个与平均值比较，输出大于平均值的数*/
      if(a[i]> average)
         printf("%d ", a[i]);
   }
   printf("\n");

   return 0;
}

void ex_70_basic()
{
	int a = 1; //a是一个整数变量
	double d = 2.0; //一个小数变量
	
	int b[3] = {1, 2, 3}; /* b 是一个包含 3 个整数的数组 */
	double c[5] = {1000.0, 2.0, 3.4, 7.0, 50.0}; /* c 是一个包含 5 个小数的数组 */
	
	b[0] = 11;
	b[1] = 12;
	b[2] = 13;
	c[0] = 50.0;
	c[4] = 1000.0;
	
	int n[4]; /* n 是一个包含 4 个整数的数组 */
	int i,j;
 
	/* 初始化数组元素 */         
	for ( i = 0; i < 4; i++ )
	{
		n[ i ] = i + 100; /* 设置元素 i 为 i + 100 */
	}
   
	/* 输出数组中每个元素的值 */
	for (j = 0; j < 4; j++ )
	{
		printf("Element[%d] = %d\n", j, n[j] );
	}
}

int main(void)
{
	int choice, i;

	for (i = 1; i <= 15; i++) {/* ---------for 的循环体语句开始-------- */
		printf("Enter choice: ");             /* 输入提示 */
		scanf("%d", &choice);                 /* 开关打向哪个函数 */
		rewind(stdin); //rewind函数是把指定流的读写指针重新指向开头。比如scanf或getchar
											  
		switch (choice) {
			case 70:  ex_70_basic(); break;				//李继飞添加，书上无。 数组的基本概念
			case 71:  ex_71_average(); break;          /*【例7-1】输入10个整数，计算这些数的平均值，再输出所有大于平均值的数。*/
			case 72:  ex_72_Fibonacci(); break;        /* 【例7-2】利用数组计算斐波那契数列的前10个数 */
			case 73:  ex_73_find_number(); break;      /* 在数组中查找一个给定的数 */
			//case 731: ex_76_find_number(); break;      //看时间    /*二分查找法*/
			case 74:  ex_74_min_array(); break;        /* 找出数组的最小值和它所对应的下标 */   //后面添加两个数交换，两个数组元素交换
			case 75:  ex_75_sort_select(); break;      /* 选择法排序 */
			case 76:  ex_76_vote(); break;             /* 投票情况统计 */
			case 77:  ex_77_max_2d_array(); break;     //先讲78    /* 找出矩阵中的最大值及其行下标和列下标 */
			case 78:  ex_78_printf_2d_array(); break;  /* 按矩阵的形式输出二维数组 */
			case 79:  ex_79_transpose_matrix(); break; /* 方阵转置 */
			case 710: day_of_year(2020,11,18); break;  /* 计算某个日期对应该年的第几天 */
			case 711: ex_711_palindrome(); break;      //先讲712   /*判断字符串是否为回文*/
			case 712: ex_712_count(); break;           /* 统计字符串中数字字符的个数 */
			case 713: ex_713_extract_number(); break;  /*字符串提取其中的所有数字字符（'0'……'9'）*/
			case 714: ex_714_filter_hex(); break;      /* 滤去非十六进制字符组成一个新的表示十六进制数字的字符串*/
		}
	}                         /* ---------for 的循环体语句结束---------*/
	
	return 0;
}