/*【例11-1】已知奥运五环的5种颜色的英文单词按一定顺序排列，输入任意一个颜色的英文单词，从已有颜色中查找并输出该颜色的位置值，若没有找到，则输出"Not Found"。*/

/* 查找奥运五环色的位置，用指针数组实现 */
#include<stdio.h>
#include<string.h>
int main()
{
   int i;
   char *p="blue";//（类型 *）告诉我们要定义一个指针来放地址了。这
         //个指针名字叫p，p得到地址初值即放在系统常量区的”blue”的地址 。
   char str[20];
   scanf("%s", str);
   int cmp = (strcmp(str, p) == 0);// strcmp是字符串比较函数
   printf("color: str %s, p %s, cmp %d", str, p, cmp);

   char *color[5] = {"red", "blue", "yellow", "green", "black" };  /* 指针数组初始化 */

   for(i = 0; i < 5; i++)
     if(strcmp(str, color[i]) == 0)  /* 比较颜色是否相同 */
       break;
   if(i < 5)
     printf("position:%d\n", i+1);
   else
     printf("Not Found\n");

   return 0;
}