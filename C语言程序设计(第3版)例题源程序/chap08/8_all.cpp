#include<stdio.h>

/* 【例8-12】先输入一个正整数n，再输入任意n个整数，计算并输出这n个整数的和。要求使用动态内存分配方法为这n个整数分配空间。*/
/*  求任意个整数和*/
# include <stdlib.h>
int ex_812_sum(void)
{
		int n, sum, i, *p;

		printf("Enter n: ");
		scanf("%d", &n);
		/*为数组p动态分配n个整数类型大小的空间 */
		if ((p = (int *)calloc(n, sizeof(int))) == NULL) {
			printf("Not able to allocate memory. \n");
			exit(1);
		}
		printf("Enter %d integers: ", n);   		/* 提示输入n个整数 */
		for (i = 0; i < n; i++)
			scanf("%d", p+i);
		sum = 0;
		for (i = 0; i < n; i++)						/* 计算n个整数和 */
			sum = sum + *(p+i);
		printf("The sum is %d \n",sum);
		free(p);									/* 释放动态分配的空间 */

		return 0;
}

/* 8-11 程序B：处理数 */
int ex_811_num( )
{
    int i;
    int x, min;    
    scanf("%d", &x);
    min=x; 
    for(i=1; i<5; i++){
        scanf("%d", &x);
        if(x< min)  
            min=x;
     }
    printf("min is %d\n", min);

    return 0;
 }    

/* 8-11 程序A：处理字符串 */
#include <string.h>
int ex_811_string( )
{
    int i;
    char sx[80], smin[80];    

    scanf("%s", sx);
    strcpy(smin,sx); 
    for(i=1; i<5; i++){
       scanf("%s", sx);
       if(strcmp(sx, smin)<0)  
             strcpy(smin,sx);
     }
    printf("min is %s\n", smin);

    return 0;
}    


/* 8-10 程序B：使用gets和puts函数输入输出字符串的示例*/
int ex_810_gets_puts( )
{ 
   char str[80];

   gets(str);
   puts(str);
   puts("Hello");

   return 0;
} 

/* 8-10 程序A：使用scanf和printf
函数输入输出字符串的示例 */
int ex_810_scanf_printf( )
{
   char str[80];

   scanf("%s", str);
   printf("%s", str);
   printf("%s", "Hello");

   return 0;
}


/*【例8-9】为了防止信息被别人轻易窃取，需要把电码明文通过加密方式变换成为密文。
   变换规则如下：小写字母z变换成为a，其他字母变换成为该字母ASCII码顺序后1位的字母，比如o变换成为p。*/
/* 密码变换问题 */
# include <string.h>
# define MAXLINE 100
void encrypt ( char *);
int ex_89_encrypt(void)
{
   char line [MAXLINE];
   
   printf ("Input the string: ");
   gets(line);
   encrypt (line);
   printf ("%s%s\n", "After being encrypted: ", line);
   return 0;
}
void encrypt ( char *s)
{
   for ( ; *s != '\0'; s++)
      if (*s == 'z')
		*s = 'a';
	  else
		*s = *s+1;
}

/* 【例8-8】二分查找。设已有一个10个元素的整形数组a，且按值从小到大有序。
输入一个整数x，然后在数组中查找x，如果找到，输出相应的下标，否则，输出"Not Found"。 */
/* 二分查找 */
# include<stdio.h>
int Bsearch(int *p, int n, int x);      /* 函数声明 */
int ex_88_b_search(void)   
{
	int a[10] = {1,2,3,4,5,6,7,8,9,10};     /* 有序数组 */
    int x, m;

    printf("Enter x：");            /* 提示输入x */
    scanf("%d",&x);                
	m = Bsearch(a, 10, x);
    if(m >= 0)   
        printf("Index is %d \n",m);
    else 
        printf( "Not Found\n");
		
    return 0;
}
int Bsearch(int *p, int n, int x)   /* 二分查找函数 */
{   
	int low, high, mid;
	low = 0; high = n - 1;       /* 开始时查找区间为整个数组 */
    while (low <= high)  {          /* 循环条件 */
        mid = (low + high) / 2;     /* 中间位置 */
        if (x == p[mid])
            break;                  /* 查找成功，中止循环 */
        else if (x < p[mid])
            high = mid - 1;         /* 前半段，high前移 */ 
        else       
            low = mid + 1;          /* 后半段，low后移 */
    }    
    if(low <= high)   
        return mid;                 /* 找到返回下标 */
    else 
        return -1;                  /* 找不到返回-1 */
}

/*【例8-7】输入4个整数作为数组元素，分别使用数组和指针来计算并输出它们的和。*/
/*  分别使用数组和指针计算数组元素之和 */
int ex_87_sum4(void)
{
	int i, a[4], *p;
	long sum = 0;

	printf("Enter 4 integers: ");
	for(i = 0; i < 4; i++) 
		scanf("%d", &a[i]);

	for ( i = 0; i < 4; i++)
		sum = sum + a[i];
	printf("calculated by array, sum=%ld \n", sum);

	sum=0; 							/* 重新初始化sum为0 */
	for(p = a; p <= a+3; p++)       /* 使用指针求和 */
		sum = sum + *p;
	printf("calculated by pointer , sum=%ld \n", sum);

	return 0;
}    

/* 【例8-6】使用指针计算数组元素个数和数组元素的存储单元数。*/
/*  指针和数组及存储单元  */
int ex_86_array_count(void)
{
    double a[2], *p, *q;

    p = &a[0];               		/* 指针p向数组a的首地址 */
    q = p + 1;               		/* 指针q指向数组元素a[1] */
    printf ("%d\n", q - p);      			/* 计算指针p和q之间的元素的个数 */
    printf ("%d\n", (int) q - (int) p);  	/* 计算指针p和q之间的字节数 */

	return 0;
}

/* 【例8-5】输入n个正整数，将它们从小到大排序后输出，要求使用冒泡排序算法。*/
/*  冒泡排序算法 */
void swap2 (int *, int *);
void bubble (int a[ ], int n);
int ex_85_bubble(void)
{    
	int n, a[8];
	int i;

	printf("Enter n (n<=8): ");
	scanf("%d", &n);
	printf("Enter a[%d] : ",n);
	for (i=0; i<n;i++)
		scanf("%d",&a[i]);

	//int b[8] = a;
	bubble(a,n);
	printf("After sorted, a[%d] = ", n);
	for (i=0; i<n; i++)
		printf("%3d",a[i]);

	return 0;
}
//void bubble (int a[ ], int n)  		/*  n是数组a中待排序元素的数量 */
void bubble (int * a, int n)
{
   int  i, j, t;
   for( i = 1; i < n; i++ )		        /*  外部循环  */
      for (j = 0; j < n-i; j++ )	    /*  内部循环  */
        if (a[j] > a[j+1]){		    	/*  比较两个元素的大小  */
          t=a[j]; a[j]=a[j+1]; a[j+1]=t;	/*  如果前一个元素大，则交换 */
		 }
}


/* 【例8-4】输入年份和天数，输出对应的年、月、日。要求定义和调用函数month_day ( year, yeardy, *pmonth, *pday)，
其中year是年，yearday是天数，*pmonth和*pday是计算得出的月和日。例如，输入2000和61，输出2000-3-1，即2000年的第61天是3月1日。
*/
/*  使用指针作为函数参数返回多个函数值的示例 */
int ex_84_year_month_day(void)
{
	int day, month, year, yearday;					/*  定义代表日、月、年和天数的变量*/
	void month_day(int year,int yearday, int *pmonth,int *pday);/*声明计算月、日的函数*/
	printf("input year and yearday: ");				/* 提示输入数据：年和天数 */
	scanf ("%d%d", &year, &yearday );		
	month_day (year, yearday, &month, &day );		/* 调用计算月、日函数  */ 
	printf ("%d-%d-%d \n", year, month, day );	

	return 0;	
}
void month_day ( int year, int yearday, int * pmonth, int * pday)
{
	int k, leap;
	int tab [2][13] = {
       {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },
       {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },
	};  /* 定义数组存放非闰年和闰年每个月的天数 */

	/* 建立闰年判别条件leap */
	leap = (year%4 == 0 && year%100 != 0) || year%400 == 0; 
   
	for ( k = 1; yearday > tab[leap][k]; k++)
       yearday -= tab [leap][k];
	*pmonth = k;
	*pday = yearday;
}

/* 【例8-3】有两个角色分别用变量a和b表示。为了实现角色互换，现制定了3套方案，
    通过函数调用来交换变量a和b的值，即：swap1()、swap2()和swap3()。请分析在swap1()、
	swap2()和swap3()这3个函数中，哪个函数可以实现这样的功能。
*/
/*  通过函数调用来交换变量值的示例程序 */
int ex_83_swap(void)
{
	int a = 1, b = 2;
	int *pa = &a, *pb = &b;
	void swap1(int x, int y), swap2( int *px, int *py ), swap3 (int *px, int *py);
   
	swap1 (a, b);    				/* 使用变量a，b调用函数swap1() */
	printf ("After calling swap1: a=%d b=%d\n", a, b);

	a = 1; 
	b = 2;
	swap2(pa, pb);     				/* 使用指针pa，pb调用函数swap2()*/
	printf ("After calling swap2: a=%d b=%d\n", a, b);

	a = 1; 
	b = 2;
	swap3(pa, pb);     				/* 使用指针pa，pb调用swap3() */
	printf ("After calling swap3: a=%d b=%d\n", a, b);

	return 0;
}
void swap1 (int x, int y)
{
	int t;

	t= x; 
	x = y; 
	y = t;
}
void swap2 (int *px, int *py)
{
	int t;

	t = *px; 
	*px = *py; 
	*py = t;
}
void swap3 (int *px, int *py)
{
	int *pt;

	pt =px; 
	px = py; 
	py = pt;
}

/* 【例8-2】取地址运算和间接访问运算示例  */
/*  取地址运算和使用指针访问变量 */
int ex_82_get_address(void)
{
	int a = 3, *p;            			/* 第4行：定义整型变量a和整型指针p    */

	p = &a;                 			/* 把变量a的地址赋给指针p，即p指向a   */
	printf ("a=%d, *p=%d\n", a, *p);	/* 输出变量a的值和指针p所指向变量的值*/
	*p = 10;                 			/* 对指针p所指向的变量赋值，相当于对变量a赋值 */
	printf("a=%d, *p=%d\n", a, *p);
	printf("Enter a: ");
	scanf("%d", &a);        			/* 输入a */
	printf("a=%d, *p=%d\n", a, *p);
	(*p)++;								/* 将指针所指向的变量加1  */
	printf("a=%d, *p=%d\n", a, *p);

	return 0;
}

/* 【例8-1】利用指针模拟密码开锁游戏 */
/*  获取密码的两种方法 */
int ex_81_unlock(void)
{
		int x = 5342;     /* 变量x用于存放密码值5342 */
		int *p = NULL;   /* 定义整型指针变量p，NULL值为0，代表空指针 */

		p = &x;        /*将变量x的地址存储在p中 */

		/* 通过变量名x输出密码值*/
		printf("If I know the name of the variable, I can get it's value by name: %d\n ", x);   
	    /* 通过变量x的地址输出密码值 */
		printf("If I know the address of the variable is: %x, then I also can get it's value by address: %d\n",p, *p);

		return 0;
}

#define MAXS 10
void GetString( char s[] )
{
	gets(s);
}
//void GetString( char s[] ); /* 实现细节在此不表 */

//其中char s[]是用户传入的字符串，题目保证其长度不小于3；函数Shift须将按照要求变换后的字符串仍然存在s[]里
void Shift( char s[] ){
	
	
}

int lianxi_88_shift()
{
    char s[MAXS];

    GetString(s);
    Shift(s);
    printf("%s\n", s);

    return 0; 
}

//其中list[]是用户传入的数组；n（≥0）是list[]中元素的个数；x是待查找的元素。如果找到
//则函数search返回相应元素的最小下标（下标从0开始），否则返回?1。
int search( int list[], int n, int x )
{
	
	
	//下面这个return 0要改成return 题目要求的值。
	return 0;
}

#define MAXN 10
int xiti_82_search()
{
    int i, index, n, x;
    int a[MAXN];

    scanf("%d", &n);
    for( i = 0; i < n; i++ )
        scanf("%d", &a[i]);
    scanf("%d", &x);
    index = search( a, n, x );
    if( index != -1 )
        printf("index = %d\n", index);
    else
        printf("Not found\n");

    return 0;
}

//其中op1和op2是输入的两个实数，*psum和*pdiff是计算得出的和与差。
void sum_diff( float op1, float op2, float *psum, float *pdiff )
{
	
	
}

int lianxi_82_sum_diff()
{
    float a, b, sum, diff;

    scanf("%f %f", &a, &b);
    sum_diff(a, b, &sum, &diff);
    printf("The sum is %.2f\nThe diff is %.2f\n", sum, diff);

    return 0; 
}

int main(void)
{
	int choice, i;

	for (i = 1; i <= 14; i++) {		/************for 的循环体语句开始*****************/
		printf("Enter choice: ");             /* 输入提示 */
		scanf("%d", &choice);                 /* 开关打向哪个函数 */
		rewind(stdin); //本函数与指针无关请忽视。 （rewind函数是把指定流的读写指针重新指向开头。比如scanf或getchar）
											  
		switch (choice) {
			case 81:   ex_81_unlock(); break;           /* 【例8-1】利用指针模拟密码开锁游戏 */
			case 82:   ex_82_get_address(); break;      /* 【例8-2】取地址运算和间接访问运算示例  */
			case 83:   ex_83_swap(); break;      		/*  通过函数调用来交换变量值的示例程序 */
			case 84:   ex_84_year_month_day(); break;   /* 【例8-4】输入年份和天数，输出对应的年、月、日*/
			 case 801:  lianxi_82_sum_diff(); break;     	/////练习8-2	计算两数的和与差
			case 85:   ex_85_bubble(); break;      		/*  冒泡排序算法 */
			case 86:   ex_86_array_count(); break;      /* 【例8-6】使用指针计算数组元素个数和数组元素的存储单元数。*/
			case 87:   ex_87_sum4(); break;     		/*【例8-7】输入10个整数作为数组元素，分别使用数组和指针来计算并输出它们的和。*/
			case 88:   ex_88_b_search(); break; 		/* 二分查找 */
			 case 802:  xiti_82_search(); break;     		/////习题8-2	在数组中查找指定元素
			case 89:   ex_89_encrypt(); break; 			/* 密码变换问题 */
			case 8101: ex_810_gets_puts(); break;  		/* 使用gets和puts函数输入输出字符串的示例*/
			case 8102: ex_810_scanf_printf(); break;    /* 使用scanf和printf*/
			case 8111: ex_811_string(); break;          /* 8-11 程序A：处理字符串 */
			case 8112: ex_811_num(); break;  			/* 8-11 程序B：处理数 */
			 case 803:  lianxi_88_shift(); break;     		//练习8-8	移动字母
			case 812:  ex_812_sum(); break;      		/*  求任意个整数和*/
		}
	}                         		/*************for 的循环体语句结束*****************/
	
	return 0;
}