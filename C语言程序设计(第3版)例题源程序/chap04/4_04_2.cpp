/* 【例4-4-2】输入一个正整数m，判断它是否为素数。素数就是只能被1和自身整除的正整数，1不是素数，2是素数。*/

/* 判断正整数m是否为素数 */
#include <stdio.h>
int main(void)
{
    int i, flag, m;

    flag = 1;
    printf("Enter a number: ");  /* 输入提示 */
    scanf ("%d", &m);	
    if ( m == 1) flag = 0; 
    for(i = 2; i <= m / 2; i++)  
        if(m % i == 0){
        	flag = 0;
            break;               /* 若m能被某个i整除，则m不是素数，提前结束循环 */
        }
     
    if(flag == 1)      
        printf("%d is a prime number! \n", m);  
    else
        printf("No!\n"); 

    return 0;
}
