/* crashbox.dat是随机文件，记录资金账户消费流水帐记录信息 */
/* 程序的功能：添加新流水帐记录，查询资金账户最后余额 */

#include "stdio.h"
#include "stdlib.h"
#include "process.h"
long size;						/*当前最近一次的流水号*/
struct LogData{					/*记录的结构*/
	long logid;					/*记录ID*/
    char logdate[11];			/*记录发生日期*/
    char lognote[15];			/*记录事件说明*/
    double charge;				/*发生费用：负表示支出，正表示收入*/
    double balance;				/*余额*/
};

int inputchoice()					/*选择操作参数*/
{
	int mychoice;
	
	printf("\nEnter your choice:\n");
	printf("1 - Add a new cash LOG.\n2 - List All Cash LOG.\n");
	printf("3 - Query Last Cash LOG.\n0 - End program.\n");
	scanf("%d",&mychoice);
	return mychoice;
}

long getLogcount(FILE *cfptr) 		/*获取文件记录总数*/
{  
	long begin,end,logcount;
	
	fseek(cfptr,0L,SEEK_SET);
	begin=ftell(cfptr);
	fseek(cfptr,size,SEEK_END);
	end=ftell(cfptr);
	logcount=(end-begin)/size-1;
	return logcount;
}

void ListAllLog(FILE *cfptr) 		/*列出所有收支流水帐*/
{
	struct LogData log;

	fseek(cfptr,0L,SEEK_SET);			/*定位指针到文件开始位置*/
	fread(&log,size,1,cfptr);
	printf("logid  logdate   lognote             charge    balance\n");
	while(!feof(cfptr)){
		printf("%6ld %-11s %-15s %10.2lf %10.2lf\n",
		log.logid,log.logdate,log.lognote,log.charge,log.balance);
		fread(&log,size,1,cfptr);
	};
}

void QueryLastLog(FILE *cfptr) 		/*查询显示最后一条记录*/
{
	struct LogData log;
	long logcount;
	
	logcount=getLogcount(cfptr);
	if(logcount>0)						/*表示有记录存在*/
	{
		fseek(cfptr,size*(logcount-1),SEEK_SET);/*定位最后记录*/
	    fread(&log,size,1,cfptr);		/*读取最后记录*/
	    printf("The last log is:\n");
	    printf("logid:%-6ld\nlogdate:%-11s\nlognote:%-15s\n",log.logid,log.logdate,log.lognote);
		printf("charge:%-10.2lf\nbalance:%-10.2lf\n",
		log.charge,log.balance);   /*显示最后记录内容*/
	}
	else  
		printf("no logs in file!\n");	
}

void AddNewLog(FILE *cfptr) 			/*添加新记录*/
{
		struct LogData log,lastlog;
		long logcount;

        printf("Input logdate(format:2006-01-01):");
		scanf("%s",log.logdate);
		printf("Input lognote:");scanf("%s",log.lognote);
		printf("Input Charge:Income+ and expend-:");
		scanf("%lf",&log.charge);	
		logcount=getLogcount(cfptr);		/*获取记录数*/
	
		if(logcount>0){
			fseek(cfptr,size*(logcount-1),SEEK_SET);
			fread(&lastlog,size,1,cfptr);/*读入最后记录*/
			log.logid=lastlog.logid+1;	/*记录号按顺序是上次的号+1*/
			log.balance=log.charge+lastlog.balance;
		}
		else{								/*如果文件是初始，记录数为0*/
			log.logid=1;
			log.balance=log.charge;
		}
		rewind(cfptr);
		printf("logid= %ld\n",log.logid);
		fwrite(&log,sizeof(struct LogData),1,cfptr);/*写入记录*/
}

int ex_12_5_cash_system(void)
{
	FILE *fp; int choice;

	if((fp=fopen("d:\\cashbox.dat", "ab+")) == NULL){
		printf("can not open file cashbox.dat!\n");
		exit(0);
    }
	size = sizeof(struct LogData);
	while((choice=inputchoice())!=0){
		switch(choice){
			case 1:
				AddNewLog(fp);break;
			case 2:
				ListAllLog(fp);break;/*列出所有的收入支出情况*/
			case 3:
				QueryLastLog(fp);break;/*查询最后的余额*/
			default:
				printf("Input Error.");break;
		}
	}

    if(fclose(fp)){	 
		printf( "Can not close the file!\n" );
        exit(0);
    }
	return 0;
}

/* 【例12-5】编程实现以二进制方式读写用户信息文件f12-5.dat，将3位用户信息(用户名和密码，密码要求经过例12-2的encrypt()函数加密)写入文件，然后读出所有用户信息显示到屏幕。 */

#include <stdio.h>
#include <string.h>
#include<process.h>
#define SIZE 3  /*用户个数*/
struct sysuser{ /*用户信息结构体*/
	  char username[20];
	  char password[8];
};
void encrypt(char *pwd);
int ex_12_5_fwrite(void)
{
	FILE *fp;   /*1.定义文件指针*/
	int i;  
	struct sysuser u[SIZE],su[SIZE],*pu=u,*psu=su;

	/*2.打开文件，建立二进制文件进行读/写方式*/
	if((fp=fopen("d:\\f12-5.dat","wb+")) == NULL){
		printf("File open error!\n");
   		exit(0);
	}
	/*3.输入SIZE个用户信息，并对密码加密，保存到结构体数组u */
	for(i=0;i<SIZE;i++,pu++){
		printf("Enter %i th sysuser(name password):",i);
		scanf("%s%s",pu->username,pu->password);  /*输入用户名和密码*/
		encrypt(pu->password);    /*调用加密算法对密码进行加密处理*/
	}
	pu=u;
	fwrite(pu,sizeof(struct sysuser),SIZE,fp);/*写入二进制文件*/
	fflush(fp);
	rewind(fp);   /*将指针重新定位到文件首*/
	fread(psu,sizeof(struct sysuser),SIZE,fp);/*读取SIZE条数据到psu指向的结构数组*/
	for(i=0;i<SIZE;i++,psu++)
		printf("%s\t%s\n", psu->username, psu->password);
	/*关闭文件*/
	if(fclose(fp)){
		printf("Can not close the file!\n");
	    exit(0);
	}  
	return 0;
}
///*加密函数，对pwd进行加密处理*/
//void encrypt(char *pwd)
//{  
//	int i;
//	/*与15异或，实现低四位取反，高四位保持不变*/
//	for(i=0;i<strlen(pwd);i++) pwd[i] = pwd[i]^15;
//}

/* 【例12-4】例12-2的f12-2.txt文件保存着系统用户信息，编写一个函数checkUserValid()用于登录系统时校验用户的合法性。检查方法是在程序运行时输入用户名和密码，然后在文件中查找该用户信息，如果用户名和密码在文件中找到，则表示用户合法，返回1，否则返回0。程序运行时，输入一个用户名和密码，调用checkUserValid()函数，如果返回1，则提示"Valid user!"，否则输出"Invalid user!"。 */

/*用户合法性校验*/
#include <stdio.h>
#include <stdlib.h>
#include <process.h>
#include <string.h>
//struct sysuser{                   /*定义系统用户信息结构*/
//char username[20]; 
//char password[8];			
//};
void encrypt(char *pwd);
int checkUserValid(struct sysuser *psu);
int ex_12_4_check_user(void)
{  
	struct sysuser su;
    printf("Enter username:");  
	scanf("%s", su.username); /*输入待校验的用户名*/
    printf("Enter password:");  
	scanf("%s",su.password); /*输入待校验的密码*/
	if(checkUserValid(&su)==1)     /*调用函数进行校验*/
        printf("Valid user!\n");	   /*成功输出Valid user!*/
    else
        printf("Invalid user!\n");      /*失败输出Invalid user!*/
	return 0;
}
///*加密算法*/
//void encrypt(char *pwd)
//{
//	int i;
///*与15异或，实现低四位取反，高四位保持不变*/
//	for(i=0;i<strlen(pwd);i++) pwd[i] = pwd[i]^15;
//}

/*校验用户信息的合法性，成功返回1，否则返回0*/
int checkUserValid(struct sysuser *psu)
{
    FILE *fp;
    char usr[30],usr1[30],pwd[10];
    int check=0;            /*检查结果变量，初始化为0*/
	/*连接生成待校验字符串*/
    strcpy(usr,psu->username);    /*复制psu->username到usr1 */
    strcpy(pwd,psu->password);   /*复制psu->password到pwd */
    encrypt(pwd);              /*调用例12-2的encrypt对密码进行加密*/
	/*连接usr、空格、pwd和\n构成新字符串usr，用于在文件中检查匹配*/
    strcat(usr, " "); strcat(usr,pwd); strcat(usr,"\n");                
    /*打开文件"f12-2.txt"读入*/
    if((fp=fopen("d:\\f12-2.txt","r"))==NULL){
		printf("File open error!\n");
		exit(0);
    }
    /*从文件读入用户信息数据，遍历判断是否存在*/
    while(!feof(fp)){
		fgets(usr1,30,fp);         /*读入一行用户信息字符串到usr1*/
		if(strcmp(usr,usr1)==0){   /*比较判断usr与usr1是否相同*/
			check=1; break;
		}
	}
	/*关闭文件*/
	if(fclose(fp)){
		printf("Can not close the file!\n");
		exit(0);
	}
	return check;
}

/* 【例12-3】复制用户信息文件。将例12-2的用户信息文件f12-2.txt文件备份一份，取名为文件f12-3.txt。说明：将文件f12-2.txt与源程序放在同一目录下，执行程序。 */

#include <stdio.h>
#include <process.h>
int ex_12_3_copy_file(void)
{
   FILE *fp1,*fp2;
   char ch;
	   /*打开文件，读出数据*/
   if((fp1=fopen("d:\\f12-2.txt","r"))==NULL){
      printf("File open error!\n");
      exit(0);
   }
	   /*打开文件，写入数据*/
   if((fp2=fopen("d:\\f12-3.txt","w"))==NULL){
      printf("File open error!\n");
      exit(0);
   }
   /*复制数据，从文件1读取，写入文件2 */
   while(!feof(fp1)){
      ch=fgetc(fp1);  /*从fp1所指示的文件中读取一个字符*/
	  if(ch!=EOF) {
		  fputc(ch,fp2);  /*将字符ch写入fp2指示的文件*/
		  fflush(fp2);
	  }
   }
   /*关闭文件f12-2.txt */
   if(fclose(fp1)){
	    printf("Can not close the file!\n");
	    exit(0);
	}
	/*关闭文件f12-3.txt */
	if(fclose(fp2)){
	    printf("Can not close the file!\n");
	    exit(0);
	}
	return 0;
}

/*【例12-2】为了保障系统安全，通常采取用户帐号和密码登录系统。系统用户信息存放在一个文件中，系统帐号名和密码由若干字母与数字字符构成，因安全需要文件中的密码不能是明文，必须要经过加密处理。请编程实现：输入3个用户信息（包含帐号名和密码）并写入文件f12-2.dat。要求文件中每个用户信息占一行，帐号名和加密过的密码之间用一个空格分隔。密码加密算法：对每个字符ASCII码的低四位求反，高四位保持不变（即将其与15进行异或）。*/

/*创建系统用户帐号信息文件存储用户名和加密的密码*/
#include <stdio.h>
#include <string.h>
#include <process.h>
//struct sysuser{                   /*定义系统用户帐号信息结构*/
//	char username[20]; 
//	char password[8];			
//};
int ex_12_2_write_file(void)
{
	FILE *fp;          			/*1.定义文件指针*/
	int i;
	void encrypt(char *pwd);
	struct sysuser su;  
								/*2.打开文件，进行写入操作*/
	if((fp=fopen("d:\\f12-2.txt","w")) == NULL){	 
   	    printf("File open error!\n");
   		exit(0);
	}
  								/*3. 将5位用户帐号信息写入文件*/
	for(i=1;i<=3;i++){
		printf("Enter %i th sysuser(name password):",i);
		scanf("%s%s",su.username,su.password); 			/*输入用户名和密码 */
		encrypt(su.password);    						/*进行加密处理*/
		fprintf(fp,"%s %s\n",su.username,su.password); 	/*写入文件*/
		fflush(fp);
	}
	if(fclose(fp)){				/*4.关闭文件*/
		printf("Can not close the file!\n");
		exit(0);
	}	
	return 0;
}	
/*加密算法*/
void encrypt(char *pwd)
{
	int i;
/*与15（二进制码是00001111）异或，实现低四位取反，高四位保持不变*/
	for(i=0;i<strlen(pwd);i++) 
		pwd[i] = pwd[i] ^ 15;
}


/*【例12-1】有5位学生的计算机等级考试成绩被事先保存在数据文件C:\f12-1.txt(C盘根目录下的文件f12-1.txt，需事先准备好该文件)中，包括学号、姓名和分数，文件内容如下： 
 301101 Zhangwen 91
301102 Chenhui 85
301103 Wangweidong 76
301104 Zhengwei 69
301105 Guowentao 55
请读出文件的所有内容显示到屏幕，并输出平均分。
*/
#include <stdio.h>
#include <stdlib.h>
int ex_12_1_read_file(void)
{
   FILE *fp;                    			/*1.定义文件指针*/
   long num;
   char stname[20];
   int score;
   int i, sum_score = 0;

   if((fp=fopen("d:\\f12-1.txt","r")) == NULL)	/*2.打开文件*/
   {
   		printf("File open error!\n");
   		exit(0);
   }
    										/*3.文件处理（逐个读入和处理数据）*/
   for(i=0;i<5;i++)
   {
	/*从文件读入成绩保存到变量*/
       fscanf(fp,"%ld%s%d",&num,stname,&score);						            
       sum_score += score;    /*统计总分*/
      /*输出成绩到屏幕*/
       printf("%ld	%s %d\n",num,stname,score);  
   }

   /*输出平均分到屏幕*/
	printf("Average score: %d\n", sum_score/5);  			
	if(fclose(fp)){	 						/*4.关闭文件*/
       printf( "Can not close the file!\n" );
       exit(0);
   }
	return 0;
}

int main(void)
{
	int choice, i;

	for (i = 1; i <= 5; i++) {		/************for 的循环体语句开始*****************/
		printf("Enter choice: ");             /* 输入提示 */
		scanf("%d", &choice);                 /* 开关打向哪个函数 */
		rewind(stdin); //本函数与本章无关请忽视。 （rewind函数是把指定流的读写指针重新指向开头。比如scanf或getchar）
		
		switch (choice) {
			case 121:  ex_12_1_read_file();   break;	/*读出文件的所有内容显示到屏幕，并输出平均分。 */
			case 122:  ex_12_2_write_file();  break;	/*创建系统用户帐号信息文件存储用户名和加密的密码*/
			case 123:  ex_12_3_copy_file();  break;		/* 【例12-3】复制用户信息文件 */
			case 124:  ex_12_4_check_user();  break;	/*用户合法性校验*/
			case 125:  ex_12_5_fwrite();  break;		/*将5位用户信息(用户名和密码)写入文件*/
			case 126:  ex_12_5_cash_system();  break;		/*将5位用户信息(用户名和密码)写入文件*/
		}
	}                         		/*************for 的循环体语句结束*****************/
	
	return 0;
}
